package com.example.practica0292;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btnIMC;
    private Button btnBorrar;
    private Button btnTerminar;
    private EditText txtAlto;
    private EditText txtKilos;
    private TextView txtIMC2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Relacionar los objetos
        btnIMC = (Button) findViewById(R.id.btnCalcular);
        btnBorrar = (Button) findViewById(R.id.btnLimpiar);
        btnTerminar = (Button) findViewById(R.id.btnCerrar);
        txtAlto = (EditText) findViewById(R.id.txtAltura);
        txtKilos = (EditText) findViewById(R.id.txtPeso);
        txtIMC2 = (TextView) findViewById(R.id.txtIMC);

        //Codificar el boton Calcular
        btnIMC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Tomar el texto de los EditText (texto escrito por el usuario)
                String txtAlturaU = txtAlto.getText().toString();
                String txtPesoU = txtKilos.getText().toString();


                //Otra manera de validar un campo requerido
                /*// Validar que txtAltura está vacío
                if (txtAlturaU.isEmpty()) {
                    txtAlto.setError("Este campo es requerido");*/

                if (txtAlturaU.matches("") || txtPesoU.matches("")) {
                        //Requerir campos
                        Toast.makeText(MainActivity.this, "Todos los campos son requeridos",
                                Toast.LENGTH_LONG).show();


                } else {
                    // Tomar los valores altura y peso
                    double altura = Double.parseDouble(txtAlto.getText().toString());
                    double peso = Double.parseDouble(txtKilos.getText().toString());

                    // Validar que los valores no sean 0
                    if(altura!=0 || peso!=0){
                        // Se convierte la altura a metros
                        altura=altura/100;
                        //Se calcula el imc = peso/altura^2
                        double imc = peso / (altura * altura);

                        // Impresión del imc
                        txtIMC2.setText("Su IMC es: " + imc);

                    // Validar si hay campos con 0
                    }else {
                        Toast.makeText(MainActivity.this, "Introduzca valores mayores a 0",
                                Toast.LENGTH_LONG).show();
                        btnBorrar.callOnClick();
                    }

                }


            }
        });



        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtAlto.setText("");
                txtKilos.setText("");
                txtIMC2.setText("Su IMC es:");
            }
        });



        btnTerminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



    }


}
